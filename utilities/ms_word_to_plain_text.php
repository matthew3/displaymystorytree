<?php
	include('/srv/http/storytree.displaymy.com/public_html/php-scripts/functions/database.php');
	include('/srv/http/storytree.displaymy.com/public_html/php-scripts/classes/BatchQuery.php');
	
	$link = openDatabase();
	
	//take care of trunks
	$q = new BatchQuery($link);
	$q->addQuery('SELECT id, trunk FROM stories');
	$trunks = $q->execute();
	
	unset($q);
	
	foreach ($trunks as $t)
	{
		// replace Microsoft Word version of single  and double quotations marks (� � � �) with  regular quotes (' and ")
		$plainText = iconv('UTF-8', 'ASCII//TRANSLIT', $t['trunk']);
		$q2 = new BatchQuery($link);
		$q2->addParamQuery("UPDATE stories SET trunk=? WHERE id=?",
										'si', array($plainText, $t['id']));
		$q2->execute();
		unset($q2);
	}
	
	//take care of branches
	$q = new BatchQuery($link);
	$q->addQuery('SELECT id, content FROM branches');
	$branches = $q->execute();
	
	unset($q);
	
	foreach ($branches as $b)
	{
		// replace Microsoft Word version of single  and double quotations marks (� � � �) with  regular quotes (' and ")
		$plainText = iconv('UTF-8', 'ASCII//TRANSLIT', $b['content']);
		$q2 = new BatchQuery($link);
		$q2->addParamQuery("UPDATE branches SET content=? WHERE id=?",
										'si', array($plainText, $b['id']));
		$q2->execute();
		unset($q2);
		
	}
	
	unset($q2);
	
	mysqli_close($link);
	
	exit();
?>