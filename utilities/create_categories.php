<?php
	include('/srv/http/storytree.displaymy.com/public_html/php-scripts/functions/database.php');
	include('/srv/http/storytree.displaymy.com/public_html/php-scripts/classes/BatchQuery.php');
	
	$link = openDatabase();
	
	$q = new BatchQuery($link);
//	$q->addQuery("INSERT INTO categories VALUES (0, 'None', NULL)");
		$q->addQuery("INSERT INTO categories VALUES (1, 'Action', 0)");
		$q->addQuery("INSERT INTO categories VALUES (2, 'Adventure', 0)");
		$q->addQuery("INSERT INTO categories VALUES (3, 'Comedy', 0)");
		$q->addQuery("INSERT INTO categories VALUES (4, 'Crime', 0)");
		$q->addQuery("INSERT INTO categories VALUES (5, 'Fantasy', 0)");
		$q->addQuery("INSERT INTO categories VALUES (6, 'Historical', 0)");
		$q->addQuery("INSERT INTO categories VALUES (7, 'Horror', 0)");
		$q->addQuery("INSERT INTO categories VALUES (8, 'Mystery', 0)");
		$q->addQuery("INSERT INTO categories VALUES (9, 'Paranoid', 0)");
		$q->addQuery("INSERT INTO categories VALUES (10, 'Philosophical', 0)");
		$q->addQuery("INSERT INTO categories VALUES (11, 'Political', 0)");
		$q->addQuery("INSERT INTO categories VALUES (12, 'Realistic', 0)");
		$q->addQuery("INSERT INTO categories VALUES (13, 'Romance', 0)");
		$q->addQuery("INSERT INTO categories VALUES (14, 'Satire', 0)");
		$q->addQuery("INSERT INTO categories VALUES (15, 'Science Fiction', 0)");
		$q->addQuery("INSERT INTO categories VALUES (16, 'Slice of Life', 0)");
		$q->addQuery("INSERT INTO categories VALUES (17, 'Thriller', 0)");
		
	$q->execute();
	
	$link->close();
	unset($q);
?>
