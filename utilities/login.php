<?php
	$some_name = session_name('displaymy');
	include("functions/database.php");
	require_once("classes/BatchQuery.php");
	
	function addError($label, $str)
	{
		if (!isset($_SESSION['temp']))
		{
			$_SESSION['temp'] = array();
		}
		$_SESSION['temp'][] = $str;
	}

	function redirectIfErrors($databaseLink)
	{
		if (isset($_SESSION['temp']))
		{
			$_SESSION['login_errors'] = array();
			$_SESSION['login_errors'] = $_SESSION['temp'];
			unset($_SESSION['temp']);
			if ($databaseLink != null)
				$databaseLink->close();
			exit('There was an error logging in. Sorry for the inconvenience.');
			//exit('No javascript enabled... Click <a href="' . $_SERVER['HTTP_REFERER'] . '">here</a><script type="text/javascript">window.location.history.go(-1);</script>');
			//redirect back to registration page
		}
	}
	
	$user_email = $_POST['user_email'];
	$inputPass = $_POST['pass'];
	
	$link = openDatabase();
	
	$q = new BatchQuery($link);
	$q->addParamQuery("SELECT u.id AS id, u.user AS user, u.first_name AS first_name, u.last_name AS last_name, u.birth_date AS birth_date, u.email AS email, u.pass AS pass, u.salt AS salt,
									a.activated AS activated FROM displaymy_db.users as u, displaymy_db.activation AS a WHERE (user=? OR email=?) AND u.id = a.user_id LIMIT 1",
						'ss', array($user_email, $user_email));
	$found_user =$q->execute();
	
	if ($q->anyErrors())
	{
		addError('database','Could not retrieve user information. Please try again later.');
	}
	
	unset($q);
	
	redirectIfErrors($link);
	
	$inputCrypt = crypt($inputPass, $found_user[0]['salt']); //find encrypted password
	$realPass = $found_user[0]['pass'];
	
	if ($inputCrypt === $realPass) //compare both encrypted data
	{
		//PASS
		//start new session
		//session_set_cookie_params(0, '/', '.displaymy.com');
		session_start();
		
		$_SESSION['logged_in'] = true;
		$_SESSION['user_id'] = $found_user[0]['id'];
		$_SESSION['user'] = $found_user[0]['user'];
		$_SESSION['first_name'] = $found_user[0]['first_name'];
		$_SESSION['last_name'] = $found_user[0]['last_name'];
		$_SESSION['birth_date'] = $found_user[0]['birth_date'];
		$_SESSION['email'] = $found_user[0]['email'];
		
		$_SESSION['activated'] = $found_user[0]['activated'];
	}
	
	mysqli_close($link);
	
	if (!isset($_SESSION['logged_in']))
	{
		//redirect to index or previous page
		header('Location: http://' . $_SERVER['SERVER_NAME'] . '/request_login.php', true, 302);
		exit;
	}
	
	if (isset($_POST['url']))
	{
		//if we had a location in mind, go to that one. Otherwise, go back to where we were.
		header('Location: ' . $_POST['url']);
		exit;
	}
	
	//must redirect back to previous page
	header('Location: ' . $_SERVER['HTTP_REFERER']);
	echo "You are now logged in and can post your story or tutorial.";
	exit();
?>