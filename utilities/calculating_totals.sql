UPDATE displaymy_storytree_db.stories AS s
	SET tot_branches=(SELECT COUNT(*) FROM displaymy_storytree_db.branches AS b
						WHERE s.id=b.story_id);

UPDATE
    displaymy_storytree_db.branches AS b
  JOIN
    ( SELECT 
          b1.id
        , COUNT(*) AS cnt 
      FROM 
          displaymy_storytree_db.branches AS b1
        JOIN 
          displaymy_storytree_db.branches AS b2
            ON b1.id = b2.parent_id
      GROUP BY 
          b1.id
    ) AS g
    ON g.id = b.id
SET 
    b.tot_branches = g.cnt;
