<?php
	$some_name = session_name('displaymy');
	session_set_cookie_params(0, '/', '.displaymy.com');
	session_start();
?>
<!DOCTYPE html>
<html lang="en">
	<head>
		<link rel="stylesheet" type="text/css" href="http://www.displaymy.com/css/main.css" />
		
		<link rel="stylesheet" type="text/css" href="modules/css/top-banner.css" />
		<link rel="stylesheet" type="text/css" href="modules/css/lower-banner.css" />
		<link rel="stylesheet" type="text/css" href="modules/css/bottom-banner.css" />
	
		<link rel="stylesheet" type="text/css" href="modules/control-panel/css/control-panel.css" />
		
		<link rel="icon" href="resources/favicon.ico" type="image/x-icon" sizes="16x16" />
		
		<title>Display My StoryTree - Write As One</title>
		
		<!-- general meta (google) -->
		<meta name="description" content="Write as one! Continue each other's story while you come up with the perfect ending." />
		<meta http-equiv='Content-Type' content='Type=text/html; charset=utf-8' />
		
		<!-- open graph meta (facebook) -->
		<meta property="og:title" content="Display My StoryTree - Write As One" />
		<meta property="og:type" content="website" />
		<meta property="og:image" content="<?php echo $_SERVER['DOCUMENT_ROOT'] ?>/resources/images/default.jpg" />
		<meta property="og:url" content="http://storytree.displaymy.com/index.php" />
		<meta property="og:description" content="Write as one! Continue each other's story while you come up with the perfect ending." />
		
		<!-- twitter meta -->
		<meta name="twitter:card" content="summary" />
		<meta name="twitter:url" content="http://storytree.displaymy.com/index.php" />
		<meta name="twitter:title" content="Display My StoryTree - Write As One" />
		<meta name="twitter:description" content="Write as one! Continue each other's story while you come up with the perfect ending." />
		<meta name="twitter:image" content="<?php echo $_SERVER['DOCUMENT_ROOT'] ?>/resources/images/default.jpg" />
	</head>
	<body>
		<?php
			include('modules/top-banner.php');
		?>
		
		<div class="middle-banner">	
			<div class="main_full">
				<div style="padding:10px;">
					<?php
						if (!isset($_GET['msg']))
						{ 
							if (isset($_GET['err']))
							{
					?>
								<div class="errors">
									<?php echo $_GET['err']; ?>
								</div>
					<?php
							}
						}
						else
						{
					?>
							<div class="success">
								<?php echo $_GET['msg']; ?>
							</div>
					<?php
						}
					?>
				
					<h1>Welcome to StoryTree</h1>
					<h3>What are we here for?</h3>
					<p style="font-size:16px;">
						This is a new website that allows everyone to write stories together, all while trying to come up with that perfect plot.
						Don't know where to start? Try our <a href="random.php">random</a> button in the bottom left corner,
						or start your own story by <a href="http://www.displaymy.com/registration.php">registering</a>.
					</p>
					<p style="font-size:16px;">If you're looking for a short example, try reading one of the possible endings of <a href="http://storytree.displaymy.com/story.php?id=7#57">Who Said What Again</a>.</p>
					<p>
						Be prepared for design changes while we try to revamp our look! Thank you for your interest!
					</p>
				</div>
			</div>
		</div>
		
		<?php
			include('modules/bottom-banner.php');
		?>
	</body>
</html>