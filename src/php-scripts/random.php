<?php
	include('functions/database.php');
	include('classes/BatchQuery.php');

	function addError($label, $str)
	{
		echo $str . '<br />';
		//header('Location: ../index.php');
	}

	$link = openDatabase();
	
	//select random tutorial
	$q = new BatchQuery($link);
	$q->addQuery("SELECT id FROM stories");
	$ids = $q->execute();
	
	if ($q->anyErrors())
	{
		addError('database', 'Could not retrieve stories, please try again later.');//'Could not update the section. Please try again later.');
	}
	
	unset($q);

	$numStories = count($ids);
	
	$randStory = $ids[ rand(0, $numStories - 1) ]['id'];
	
	//select random tutorial
	$q = new BatchQuery($link);
	$q->addParamQuery("SELECT id FROM branches where story_id=?", 'i', array($randStory));
	$ids = $q->execute();
	
	if ($q->anyErrors())
	{
		addError('database', 'Could not retrieve stories, please try again later.');//'Could not update the section. Please try again later.');
	}
	
	unset($q);

	$numBranches = count($ids);
	
	$randBranch = $ids[ rand(0, $numBranches - 1) ]['id'];
	
	mysqli_close($link);
	
	header('Location: http://' . $_SERVER['SERVER_NAME'] . '/story.php?id=' . $randStory . '#' . $randBranch);
?>