<?php
	$some_name = session_name('displaymy');
	session_set_cookie_params(0, '/', '.displaymy.com');
	session_start();
	
	include('functions/database.php');
	require_once('classes/BatchQuery.php');
	
	include('functions/sanitization.php');
	include('functions/constants.php');
	
	function addError($label, $str)
	{
		if (!isset($_SESSION['temp']))
		{
			$_SESSION['temp'] = array();
		}
		$_SESSION['temp'][] = $str;
		echo $str . "<br />";
	}
	
	function redirectIfErrors($databaseLink)
	{
		if (isset($_SESSION['temp']))
		{
			$_SESSION['new_trunk_errors'] = array();
			$_SESSION['new_trunk_errors'] = $_SESSION['temp'];
			unset($_SESSION['temp']);
			if ($databaseLink != null)
			{
				mysqli_close($databaseLink);
			}
			exit();
		}
	}
	
	//ensure user is logged in
	include('functions/restriction.php');
	ensure_user_login(true, 'http://' . $_SERVER['SERVER_NAME'] . '/new_section.php');
	
	$winnie = $_POST['winnie'];
	$storyID = $_POST['story_id'];
	$trunk = $_POST['trunk'];
	
	//Most likely a bot (hidden field was filled in)
	if (strlen($winnie) != 0)
	{
		header('Location: catch_bot.php');
		exit;
	}
	
	if (strlen($trunk) > 10000)
	{
		addError('content_long', 'Your entry is too long. Please make it shorter');
	}
	
	redirectIfErrors(null);
	
	$trunk = normalize_text($trunk);
	
	$link = openDatabase();
	
	//Insert new tutorial section
	$q = new BatchQuery($link);
	$q->addParamQuery("UPDATE stories SET trunk=? WHERE id=? AND author_id=?",
						'sii', array($trunk, $storyID, $_SESSION['user_id']));
	$q->execute();
	
	if ($q->anyErrors())
	{
		addError('database', 'Could not create the story trunk. Please try again later.');
	}
	
	unset($q);
	
	redirectIfErrors($link);
	
	mysqli_close($link);
	
	echo $trunk; //TODO output a div containing the content that the user had just uploaded.
	header('Location: http://' . $_SERVER['SERVER_NAME'] . '/story.php?id=' . $storyID);
	exit;
?>