<?php
	$some_name = session_name('displaymy');
	session_set_cookie_params(0, '/', '.displaymy.com');
	session_start();
	
	include('functions/database.php');
	require_once('classes/BatchQuery.php');
	
	include('functions/constants.php');
	
	$storyID = $_POST['story_id'];
	$parentID = $_POST['parent_id'];
	
	$link = openDatabase();
	
	//check to see if the desired branch is an ending, if so, stop searching now
	$q = new BatchQuery($link);
	$q->addParamQuery("SELECT id FROM branches WHERE id=? AND story_id=? AND is_ending=1", 'ii', array($parentID, $storyID));	
	$endingIDs = $q->execute();
	
	if (count($endingIDs) > 0)
	{
		$isEnding = 1;
	}
	else //no such parent branch (0), therefore not an ending
	{
		$isEnding = 0;
	}
	
	if ($q->anyErrors())
	{
		mysqli_close($link);
		echo 'There was an error retrieving the next piece of the story. Please try again later.';
		exit();
	}
	
	unset($q);
	
	//quit if the branch specified is an ending
	if ($isEnding == '1')
	{
		echo $isEnding;
		exit();
	}
	
	/*get all tutorials that have a certain parent ID*/
	$q = new BatchQuery($link);
	$q->addParamQuery("SELECT b.id, b.content, b.author_id, b.is_ending, b.date_created, b.rating, b.views, b.tot_branches, u.user AS author_name FROM branches AS b, displaymy_db.users AS u WHERE u.id=b.author_id AND parent_id=? AND story_id=? ORDER BY rating DESC, date_created", 'ii', array($parentID, $storyID));
	$branches = $q->execute();
	
	if ($q->anyErrors())
	{
		mysqli_close($link);
		echo $q->getErrors()[0];
		exit();
	}
	
	unset($q);
	
	mysqli_close($link);
	
	//make an array of dom branches using branch id as the keys
	$branchArray = array();
	$keyArray = array();
	
	foreach ($branches as $branch)
	{
		$str = create_branch($branch['id'], $branch['author_id'], $branch['author_name'], $branch['content'], $branch['is_ending'], $branch['rating'], $branch['tot_branches']);
		
		$branchArray[] = $str;
		$keyArray[] = $branch['id'];
	}
	
	//return all the branches to story.php
	echo ('var branchArray = ' . json_encode($branchArray) . ';');
	echo ('var keyArray = ' . json_encode($keyArray) . ';');
	exit();
?>