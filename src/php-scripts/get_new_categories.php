<?php
	$some_name = session_name('displaymy');
	session_set_cookie_params(0, '/', '.displaymy.com');
	session_start();
	
	include('functions/database.php');
	require_once('classes/BatchQuery.php');
	
	$catID = $_POST['parent_id'];
	
	$link = openDatabase();
	
	/*get all tutorials that have a certain parent ID*/
	$q = new BatchQuery($link);
	$q->addParamQuery("SELECT id, description, parent_id FROM categories WHERE parent_id=?",
						'i', array($catID));		
	$categories = $q->execute();
	
	if ($q->anyErrors())
	{
		mysqli_close($link);
		echo '';
		exit();
	}
	
	if (count($categories) == 0)
	{
		mysqli_close($link);
		echo '';
		exit();
	}
	
	unset($q);
	
	$options = '';
	
	if (count($categories) > 0)
	{
		$options .= '<option value="-1">Other</option>';
	}
	
	foreach ($categories as $cat)
	{
		$options .= '<option value="' . $cat['id'] . '">' . $cat['description'] . '</option>';
	}
	
	//return all the new options discovered from the database
	mysqli_close($link);
	echo $options;
	exit();
?>