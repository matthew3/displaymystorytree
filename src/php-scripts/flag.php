<?php
	$some_name = session_name('displaymy');
	session_set_cookie_params(0, '/', '.displaymy.com');
	session_start();
	
	include('functions/database.php');
	require_once('classes/BatchQuery.php');
	
	//ensure user is logged in
	include('functions/restriction.php');
	if (!ensure_user_login(false))
	{
		echo 'false';
		exit();
	}
	
	$sectID = $_POST['section_id'];
	
	$link = openDatabase();
	
	$q = new BatchQuery($link);
	$q->addParamQuery("SELECT user_id FROM flags WHERE user_id=? AND section_id=?",
						'ii', array($_SESSION['user_id'], $sectID));
	$results = $q->execute();
	
	if ($q->anyErrors())
	{
		mysqli_close($link);
		unset($q);
		echo 'false';
		exit();
	}
	
	unset($q);
	
	//if user has already voted, exit
	if (count($results) > 0)
	{
		mysqli_close($link);
		unset($q);
		echo 'true';
		exit();
	}
	
	$q = new BatchQuery($link);
	$q->addParamQuery("INSERT INTO flags VALUES (?,?)",
						'ii', array($_SESSION['user_id'], $sectID));
	$results = $q->execute();
	
	if ($q->anyErrors())
	{
		mysqli_close($link);
		unset($q);
		echo 'false';
		exit();
	}
	
	unset($q);
	
	mysqli_close($link);
	
	echo 'true';
	exit();
?>