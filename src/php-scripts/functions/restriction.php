<?php

	function ensure_user_login($redirect, $linkTried = 'index.php')
	{
		$loggedIn = false;
		if (isset($_SESSION['logged_in']) && $_SESSION['logged_in'])
		{
			$loggedIn = true;
		}
		
		if (!$loggedIn)
		{
			if ($redirect)
			{
				echo '<script type="text/javascript">location.replace("http://www.displaymy.com/request_login.php?url=' . urlencode($linkTried) . '");</script>';
				exit();
			}
		}
		
		//logged in, but are they activated? If not...
		if ($loggedIn && $_SESSION['activated'] == 0)
		{
			if ($redirect)
			{
				echo '<script type="text/javascript">location.replace("http://www.displaymy.com/activation.php");</script>';
				exit;
			}
		}

		if ($redirect)
		{
			return '';
		}

		return $loggedIn;
	}
	
	function ensure_user_login_no_activation($redirect, $linkTried = 'index.php')
	{
		$loggedIn = false;
		if (isset($_SESSION['logged_in']) && $_SESSION['logged_in'])
		{
			$loggedIn = true;
		}
		
		if (!$loggedIn)
		{
			if ($redirect)
			{
				echo '<script type="text/javascript">location.replace("http://www.displaymy.com/request_login.php?url=' . urlencode($linkTried) . '");</script>';
				exit();
			}
		}

		if ($redirect)
		{
			return '';
		}
		
		return $loggedIn;
	}

?>
