<?php

//returns an open database object as a connection
function openDatabase()
{
	//open database
	//database error checking
	$link = new MySQLi("localhost", "root", "Em15681568", "displaymy_storytree_db");
	if ($link->connect_errno)
	{
		echo "There was a problem connecting to the database: " . $link->connect_errno . ": " . $link->connect_error;
	}
	
	return $link;
}
	
/*
Database design:

TABLE stories
-------------
id
title
trunk
author_id
date_created
primary_category
secondary_category
best_rating
reads

TABLE story_paths
------------------
story_id
best_path
worst_path
best_longest_path
worst_longest_path
best_shortest_path
best_longest_path

TABLE branches
---------------
id
text
author_id
parent_id
story_id
is_ending
rating
reads

TABLE story_changed
---------------------
story_id
changed
*/
?>
