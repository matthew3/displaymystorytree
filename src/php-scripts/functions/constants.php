<?php
	function normalize_text($text)
	{
		$text = trim($text);
	
		//convert to characters that will show instead of being considered as html
		$text = htmlspecialchars($text, ENT_QUOTES);
		//convert all new lines to <br />s
		$text = nl2br($text);
		
		$text = '<p>' . $text;
		$text = preg_replace("#(<br />)+#i", "</p><p>", $text); //change any amount of <br /> to paragraphs </p><p>
		$text = $text . '</p>';
		
		$text = preg_replace("#(<p>\\s*</p>)#i", "", $text); //erase all empty paragraph tags (even filled with space)
		
		// replace Microsoft Word version of single  and double quotations marks (“ ” ‘ ’) with  regular quotes (' and ")
		$text = iconv('UTF-8', 'ASCII//TRANSLIT', $text);
		
		return $text;
	}
	
	function create_branch($id, $author_id, $author_name, $content, $is_ending, $rating, $childCount)
	{
		$url = 'story.php?branch_id=' . $id;
	
		$str = '';
		$str .= '<div id="branch' . $id . '" class="branch">';
		$str .= '		<div class="author">';
		$str .= '			<div style="float:left;">';
		$str .= '				<a href="profile.php?id=' . $author_id . '">' . $author_name . '</a>';
		$str .= '				<span class="vote" onclick="cast_vote(' . $id . '); event.cancelBubble=true;if(event.stopPropagation) event.stopPropagation();return false;">Merit</span>(<span class="rating" id="branch_rating' . $id . '">' . $rating . '</span>)';
		$str .= '				<button onclick="set_to_branch(' . $id . ')">This Branch of Stories</button>';
		$str .= '			</div>';
		$str .= '			<div class="branch_count">';
		$str .= '				<img title="This story branch has ' . $childCount . ' continuation' . (($childCount == 1)? '' : 's') . '." src="resources/images/branch.png" width="16" height="16" /> ' . $childCount;
		$str .= '			</div>';
		$str .= '		</div>';
		
		$str .= '		<div class="content">';
		$str .= '			' . $content;
		$str .= '		</div>';
		
		if ($is_ending == '1')
		{
			$str .= '<div style="width:100%; text-align:center; color:red;">THE END</div>';
		}
		
		//add facebook like/share
		$str .= '	<fb:like href="'. $url .'" layout="button_count" action="like" show_faces="false" share="true"></fb:like>';
		
		$str .= '</div>';
		
		return $str;
	}
?>