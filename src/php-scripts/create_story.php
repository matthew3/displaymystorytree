<?php
	$some_name = session_name('displaymy');
	session_set_cookie_params(0, '/', '.displaymy.com');
	session_start();
	
	include('functions/database.php');
	require_once('classes/BatchQuery.php');
	
	include('functions/sanitization.php');
	include('functions/constants.php');
	include('functions/image.php');
	
	function addError($label, $str)
	{
		if (!isset($_SESSION['temp']))
		{
			$_SESSION['temp'] = array();
		}
		$_SESSION['temp'][] = $str;
	}
	
	function redirectIfErrors($databaseLink)
	{
		if (isset($_SESSION['temp']))
		{
			$_SESSION['new_story_errors'] = array();
			$_SESSION['new_story_errors'] = $_SESSION['temp'];
			unset($_SESSION['temp']);
			mysqli_close($databaseLink);
			header('Location: ../new_story.php');
		}
	}
	
	//ensure user is logged in
	include('functions/restriction.php');
	ensure_user_login(true, 'http://' . $_SERVER['SERVER_NAME'] .  '/new_story.php');
	
	$link = openDatabase();
	
	$winnie = $_POST['winnie'];
	$title = $_POST['title'];
	
	$title = sanitizeHTML($title);
	
	$primary_cat = 0;
	$secondary_cat = 0;
	
	if (isset($_POST['cat1']))
	{
			$primary_cat = intval($_POST['cat1']);
	}
	else
	{
		addError('prime_cat', 'Invalid primary category');
	}
	
	if ($primary_cat == 0)
	{
		addError('primary_cat', 'You must choose a category for the first one. The second one is optional');
	}
	
	if (isset($_POST['cat2']))
	{
		$secondary_cat = intval($_POST['cat2']);
	}
	
	if ($secondary_cat == $primary_cat)
	{
		$secondary_cat = 0;
	}
	
	//Most likely a bot (hidden field was filled in)
	if (strlen($winnie) != 0)
	{
		header('Location: catch_bot.php');
		exit;
	}
	
	if (strlen($title) < 1)
	{
		addError('title_length', 'This title is too short, please make it 1 character or more');
	}
	if (strlen($title) > 100)
	{
		addError('title_length2', 'This title is too long, please make it 100 characters or less');
	}
	
	redirectIfErrors($link);
	
	$title = htmlspecialchars($title, ENT_QUOTES);
	
	//create the new story
	$q = new BatchQuery($link);
	$q->addParamQuery("INSERT INTO stories VALUES (DEFAULT(id),?,'',?,NOW(),?,?,0,0,0)",
						'siii', array($title, $_SESSION['user_id'], $primary_cat, $secondary_cat));
	$q->execute();
	
	if ($q->anyErrors())
	{
		addError('database','Could not create the story. Please try again later.');
	}
	
	unset($q);
	
	$tutID = mysqli_insert_id($link);
	
	redirectIfErrors($link);
	
	mysqli_close($link);
	
	header('Location: ../story.php?id=' . $tutID);
?>