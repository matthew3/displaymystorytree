<?php
	/*
		Example usage:
		
		$link = openDatabase();
		$q = new Query($link);
		
		$q->addParamQuery('SELECT * from my_table WHERE field = ?',
									'i', array(20));
		$results = $q->execute();
		
		if (!$q->anyErrors())
		{
			echo $result['another_field'];
		}
		
		destroy $q;
	*/
	/*
		Usage of a true batch query
		
		Example usage:
		
		$link = openDatabase();
		$link->autocommit(false);
		
		$q = new Query($link);
		
		$q->addParamQuery('SELECT * from my_table WHERE field = ?',
									'i', array(20));
		$q->addParamQuery('SET @myvar := (SELECT count(*) FROM table WHERE field=?)',
									's', array($myStringVarInPHP));
		$q->addQuery(SELECT count(*) FROM table WHERE field=@myvar');
		$q->addQuery("COMMIT"); //required because we turned auto-commit off. This gives us control over what queries happen with each other. If 
		
		$results = $q->execute();
		
		if (!$q->anyErrors())
		{
			echo $result['another_field'];
		}
		
		destroy $q;
	*/
	class BatchQuery
	{
		
		private $link = null;
		
		private $queries = null;
		private $queryCount = 0;
		
		private $params = null;
		
		private $results = null;
		
		public $affectedRowCount = null;
	
		//constructor
		function __construct(&$databaseLink)
		{
			if (!$databaseLink)
				addError('invalid_link', 'The database link provided cannot be used.');
		
			$this->link = $databaseLink;
			$this->queries = array();
			$this->params = array();
			$this->results = array();
			$this->affectedRowCount = array();
			$this->queryCount = 0;
			
			$this->errors = array();
			$this->anyErrors = false;
		}
		
		function __destruct()
		{
		}
	
		/** Query setup **/
		
		//add a query string with an array of parameters ($params)
		public function addQuery($str)
		{
			if ($this->anyErrors)
				return;
			
			$this->queries[$this->queryCount] = $str;
			$this->params[$this->queryCount] = null;
		
			$this->queryCount++;
		}
		
		public function addParamQuery($query, $paramTypes, $parameters)
		{
			if ($this->anyErrors)
				return;
			
			$realParams = array();
			
			//escape each parameter, making sure they are the specified type
			$maxLen = strlen($paramTypes);
			$paramCount = 0;
			
			if ($maxLen != count($parameters))
			{
				$this->addError('unequal_params', 'There are not an equal amount of parameters defined as types declared');
				return;
			}
			
			foreach ($parameters as $param)
			{
				$correctType = false;
			
				$type = $paramTypes[$paramCount];
				
				if ($type === 'i')
				{
					$correctType = is_numeric($param);
					$realParams[$paramCount] = $this->link->real_escape_string($param);
				}
				else if ($type === 's')
				{
					$correctType = is_string($param) || is_numeric($param);
					$realParams[$paramCount] = '\'' . $this->link->real_escape_string($param) . '\'';
				}
				else if ($type === 'b')
				{
					$correctType = is_binary($param);
					$realParams[$paramCount] = $this->link->real_escape_string($param);
				}
				
				if (!$correctType)
				{
					$this->addError('type_mismatch', 'Type was specified as: ' . $type . ', but the parameter ( ' . $param . ' ) did not match.');
					return;
				}
				
				$paramCount++;
			}
			
			$this->params[$this->queryCount] = $realParams;
			$this->queries[$this->queryCount] = $query;
			
			$this->queryCount++;
		}
		
		/**************/
		
		/** Query results **/
		
		public function execute()
		{
			if ($this->anyErrors)
				return;
				
			if ($this->queryCount == 0)
			{
				addError('no_queries', 'There are no queries to execute. Try addQuery or addParamQuery');
			}
			else if ($this->queryCount == 1)
			{
				//only 1 query, execute using singular query function
				$queryString = $this->applyParameters($this->queries[0], $this->params[0]);
				
				$result = mysqli_query($this->link, $queryString);
				
				if ($this->link->errno or $result === false)
				{
					$this->addError('single_database_error', 'There was some error in the database: ' . $this->link->error . " Query: " . $queryString);
					return null;
				}
				else if ($result === true)
				{
					return null;
				}
				else
				{
					$rowCount = 0;
					while ($row = mysqli_fetch_assoc($result))
					{
						$this->results[$rowCount] = $row;
						$rowCount++;
					}
					
					mysqli_free_result($result);
				}
				
				return $this->results;
			}
			else
			{
				//more than 1 query, execute them all using multi_query
				$queryString = '';
				
				//insert all parameters and add the query string to the multi-query
				for ($i = 0; $i < $this->queryCount; $i++)
				{
					$query = $this->applyParameters($this->queries[$i], $this->params[$i]) . '; ';
					
					if ($this->anyErrors)
						return null;
					
					$queryString .= $query;
				}
				
				$ok = mysqli_multi_query($this->link, $queryString);
					
				if (!$ok)
				{
					$this->addError('multi_query_error', 'The queries did not succeed: ' . $this->link->errno . ' -- ' . $this->link->error . ' -- Query: ' . $queryString);
					return null;
				}
				
				$resultCount = 0;
				
				do
				{
					if ($this->link->errno)
					{
						$this->addError('multi_database_error', 'There was some error in the database: ' . $this->link->error);
						return null;
					}
					
					//get the result
					$res = mysqli_store_result($this->link);
					if ($res != false) //result is of a SELECT statement
					{
						$this->affectedRowCount[$resultCount] = $res->num_rows;
						
						$rowCount = 0;
						while ($r = mysqli_fetch_assoc($res))
						{
							$this->results[$resultCount][$rowCount] = $r;
							$rowCount++;
						}
						mysqli_free_result($res);
						
						$resultCount++;
					}
					else //result is of an INSERT, UPDATE, DELETE
					{
						$this->affectedRowCount[$resultCount] = mysqli_affected_rows($this->link);
						$resultCount++;
					}
					
				} while (mysqli_more_results($this->link) and mysqli_next_result($this->link));
				
				return $this->results;
			}
			
			$this->addError('no_result', 'Result from database is returned as null');
			return null;
		}
		
		/*Applies parameters in succession onto the '?' characters found in $queryString*/
		private function applyParameters($queryString, $parameters)
		{
			if ($parameters == null || empty($parameters))
			{
				return $queryString;
			}
			
			
			$retQuery = '';
		
			$len = strlen($queryString);
			$paramCounter = 0;
			$paramCount = count($parameters);
			
			for ($i = 0; $i < $len; $i++)
			{
				$char = $queryString[$i];
				if ($char == "?")
				{
					if ($paramCounter < $paramCount)
					{
						$retQuery .= $parameters[$paramCounter];
						$paramCounter++;
					}
					else
					{
						$this->addError('requires_more_params', 'The query requires more parameters than were specified');
						return '';
					}
				}
				else
				{
					$retQuery .= $char;
				}
			}
			
			return $retQuery;
		}
		
		public function freeResults()
		{
			if ($this->results != null)
			{
				if (is_array($this->results))
				{
					foreach ($this->results as $r)
					{
						mysqli_free_result($r);
					}
				}
				else
				{
					mysqli_free_result($this->results);
				}
			}
		}
		
		/***************/
		
		/** Error tracking **/
		private $anyErrors = false;
		private $errors = array();
		
		public function anyErrors()
		{
			return $this->anyErrors;
		}
		
		public function getErrors()
		{
			return $this->errors;
		}
		
		private function addError($label, $msg)
		{
			$this->errors[] = $msg;
			$this->anyErrors = true;
		}
		
		/***************/
		
	}
?>
