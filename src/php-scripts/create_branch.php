<?php
	$some_name = session_name('displaymy');
	session_set_cookie_params(0, '/', '.displaymy.com');
	session_start();
	
	include('functions/database.php');
	require_once('classes/BatchQuery.php');
	
	include('functions/sanitization.php');
	include('functions/constants.php');
	
	function addError($label, $str)
	{
		if (!isset($_SESSION['temp']))
		{
			$_SESSION['temp'] = array();
		}
		$_SESSION['temp'][] = $str;
		echo $str . "<br />";
	}
	
	function redirectIfErrors($databaseLink)
	{
		if (isset($_SESSION['temp']))
		{
			$_SESSION['new_branch_errors'] = array();
			$_SESSION['new_branch_errors'] = $_SESSION['temp'];
			unset($_SESSION['temp']);
			if ($databaseLink != null)
			{
				mysqli_close($databaseLink);
			}
			exit();
		}
	}
	
	//ensure user is logged in
	include('functions/restriction.php');
	$result = ensure_user_login(false);
	
	if (!$result)
	{
		echo 'Please <a href="http://www.displaymy.com/registration.php" target="_blank">register</a> or <a href="http://www.displaymy.com/request_login.php" target="_blank">login</a> to post.';
		exit();
	}
	
	$winnie = $_POST['winnie'];
	$storyID = $_POST['story_id'];
	$content = $_POST['content'];
	$parentID = $_POST['parent_id'];
	
	$isEnding = 0;
	if (isset($_POST['ending']) && ($_POST['ending'] == 'true'))
	{
		$isEnding = 1;
	}
	
	//Most likely a bot (hidden field was filled in)
	if (strlen($winnie) != 0)
	{
		header('Location: catch_bot.php');
		exit;
	}
	
	if (strlen($content) > 5000)
	{
		addError('content_long', 'Your entry is too long. Please make it shorter');
	}
	
	redirectIfErrors(null);
	
	$content = normalize_text($content);
	
	$link = openDatabase();
	
	//check to see if parent is an ending, if it is, we can't post a section
	$q = new BatchQuery($link);
	$q->addParamQuery("SELECT id FROM branches WHERE id=? and is_ending=1",
						'i', array($parentID));
	$parents = $q->execute();
	
	if ($q->anyErrors())
	{
		addError('database', 'Could not create the story branch. Please try again later.');
	}
	
	if (count($parents) > 0)
	{
		echo 'This story cannot be continued.';
		exit;
	}
	
	unset($q);
	
	//Insert new branch
	$q = new BatchQuery($link);
	$q->addParamQuery("INSERT INTO branches VALUES (DEFAULT(id),?,?,?,?,NOW(),?,0,0,0)",
						'siiii', array($content, $_SESSION['user_id'], $parentID, $storyID, $isEnding));
	$q->addParamQuery("UPDATE stories SET tot_branches=tot_branches+1 WHERE id=?",
						'i', array($storyID));
	$q->addParamQuery("UPDATE branches SET tot_branches=tot_branches+1 WHERE id=?",
						'i', array($parentID));
	$q->execute();
	
	if ($q->anyErrors())
	{
		addError('database', 'Could not create the story branch. Please try again later.');
	}
	
	unset($q);
	
	redirectIfErrors($link);
	
	$branchID = mysqli_insert_id($link);
	
	mysqli_close($link);
	
	$str = create_branch($branchID, $_SESSION['user_id'], $_SESSION['user'], $content, $isEnding, 0, 0);
	
	echo ($branchID . "+" . $str); //allow the caller to parse the branchID from this string
	exit;
?>