<?php
	$some_name = session_name('displaymy');
	session_set_cookie_params(0, '/', '.displaymy.com');
	session_start();
	
	include('functions/database.php');
	require_once('classes/BatchQuery.php');
	
	include('functions/sanitization.php');
	include('functions/constants.php');
	include('functions/image.php');
	
	function addError($label, $str)
	{
		if (!isset($_SESSION['temp']))
		{
			$_SESSION['temp'] = array();
		}
		$_SESSION['temp'][] = $str;
		echo $label . " --- " . $str;
	}
	
	function redirectIfErrors($databaseLink)
	{
		if (isset($_SESSION['temp']))
		{
			$_SESSION['new_tut_errors'] = array();
			$_SESSION['new_tut_errors'] = $_SESSION['temp'];
			unset($_SESSION['temp']);
			
			if ($databaseLink)
				mysqli_close($databaseLink);
			header('Location: ../new_tutorial.php');
		}
	}
	
	$winnie = $_POST['winnie'];
	$storyID = $_POST['id'];
	
	//ensure user is logged in
	include('functions/restriction.php');
	ensure_user_login(true, 'http://' . $_SERVER['SERVER_NAME'] .  '/new_story.php&id=' . $storyID . '&action=update');
	
	$primary_cat = $_POST['cat1'];
	$secondary_cat = 0;
	
	if (isset($_POST['cat1']))
	{
		if (is_integer($_POST['cat1']))
		{
			$primary_cat = $_POST['cat1'];
		}
	}
	else
	{
		addError('prime_cat', 'Invalid primary category');
	}
	
	if ($primary_cat == 0)
	{
		addError('primary_cat', 'You must choose a category for the first one. The second one is optional');
	}
	
	if (isset($_POST['cat2']))
	{
		$secondary_cat = intval($_POST['cat2']);
	}
	
	if ($secondary_cat == $primary_cat)
	{
		$secondary_cat = 0;
	}
	
	//Most likely a bot (hidden field was filled in)
	if (strlen($winnie) != 0)
	{
		header('Location: catch_bot.php');
		exit;
	}
	
	redirectIfErrors($link);
	
	$link = openDatabase();
	
	//get tutorial information for all tutorials
	$q = new BatchQuery($link);
	$q->addParamQuery("UPDATE stories SET primary_cat=?, secondary_cat=? WHERE id=?",
						'iii', array($primary_cat, $secondary_cat, $storyID));
	$storyID = $q->execute();
	
	if ($q->anyErrors())
	{
		addError('database','Could not update the tutorial. Please try again later.');
	}
	
	unset($q);
	
	redirectIfErrors($link);
	
	mysqli_close($link);
	
	header('Location: http://' . $_SERVER['SERVER_NAME'] . '/my_stories.php');
?>