<?php
	$some_name = session_name('displaymy');
	session_set_cookie_params(0, '/', '.displaymy.com');
	session_start();
	
	include('functions/database.php');
	require_once('classes/BatchQuery.php');
	
	//ensure user is logged in
	include('functions/restriction.php');
	if (!ensure_user_login(false))
	{
		echo 'Please <a href="http://www.displaymy.com/request_login.php" target="_blank">login</a> or <a href="registration.php" target="_blank">register</a> to use this feature.';
		exit();
	}
	
	$branchID = $_POST['branch_id'];
	
	$link = openDatabase();
	
	$q = new BatchQuery($link);
	$q->addParamQuery("SELECT user_id FROM votes WHERE user_id=? AND branch_id=?",
						'ii', array($_SESSION['user_id'], $branchID));		
	$results = $q->execute();
	
	if ($q->anyErrors())
	{
		mysqli_close($link);
		unset($q);
		echo 'Sorry, an error occurred.';
		exit();
	}
	
	unset($q);
	
	$link->autocommit(false);
	
	//if user already voted
	if (count($results) > 0)
	{
		mysqli_close($link);
		echo '0';
		exit();
	}
	
	/*Insert a vote and count it*/
	$q = new BatchQuery($link);
	$q->addParamQuery("INSERT INTO votes VALUES (?,?)",
						'ii', array($_SESSION['user_id'], $branchID));
	$q->addParamQuery("UPDATE branches SET rating=rating+1 WHERE id=?",
						'i', array($branchID));
	$q->addQuery("COMMIT");
	
	$results = $q->execute();
	
	if ($q->anyErrors())
	{
		mysqli_close($link);
		unset($q);
		echo 'database error.';
		exit();
	}
	
	unset($q);
	
	mysqli_close($link);
	
	echo '1';
	exit();
?>