<?php
	$some_name = session_name('displaymy');
	session_set_cookie_params(0, '/', '.displaymy.com');
	session_start();
	
	include('functions/database.php');
	require_once('classes/BatchQuery.php');
	
	include('functions/constants.php');
	
	$link = openDatabase();
	
	$origBranchID = $_POST['branch_id'];
	$branchID = $origBranchID;
	
	//get all past branches of a specified branch_id, if it is specified
	$pastBranches = array();
	if (isset($_POST['branch_id']))
	{
		$branchID = intval($_POST['branch_id']);
	
		while ($branchID != 0)
		{
			$q = new BatchQuery($link);
			$q->addParamQuery("SELECT b.id, content, author_id, u.user AS author_name, parent_id, date_created, is_ending, rating, views, tot_branches FROM branches AS b, displaymy_db.users AS u WHERE b.id=? AND b.author_id = u.id ORDER BY rating DESC", 'i', array($branchID));
			$parentBranch = $q->execute();
			
			if (count($parentBranch) > 0)
			{
				$parentBranch = $parentBranch[0];
				$pastBranches[] = $parentBranch;
				$branchID = $parentBranch['parent_id'];
			}
			else
			{
				$branchID = 0;
			}
			
			if ($q->anyErrors())
			{
				mysqli_close($link);
				echo '';//header('Location: story_not_found.php');
				exit();
			}
			
			unset($q);
		}
	}
	
	//branches were retrieved in order from bottom to top, we want to flip it so that the top is outputted first
	$pastBranches = array_reverse($pastBranches);
	
	$fullOutput = '';
	$pBranchCount = count($pastBranches);
	if ($pBranchCount > 0)
	{
		foreach ($pastBranches as $pBranch)
		{
			$str = create_branch($pBranch['id'], $pBranch['author_id'], $pBranch['author_name'], $pBranch['content'], $pBranch['is_ending'], $pBranch['rating'], $pBranch['tot_branches']);
			$fullOutput .= $str;
			echo 'idPath.push(\'' . $pBranch['id'] . '\');';
		}
		
		echo '+'; //this character can be used to separate idPath pushes from the actual text
		
		echo $fullOutput;
	}
	
	mysqli_close($link);
	
	exit();
?>