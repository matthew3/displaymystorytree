$(document).ready(function(){
	$('#cn-centre-col-inner a[href^="http://"]').each(function () {
		var t = this;
		if ($(this).find("img").length == 0 && !/privcom\.gc\.ca/.test($(this).attr('href'))) {
			//$(this).append('<span class="external"></span>');
			$(this).append('<img src="/images/icons/external-link.png" style="padding-left: 2px;" width="10" height="10" alt="external" />');
		}
	});
	$('#cn-centre-col-inner a[href^="https://"]').each(function () {
		var t = this;
		if ($(this).find("img").length == 0 && !/privcom\.gc\.ca/.test($(this).attr('href'))) {
			//$(this).append('<span class="external secure"></span>');
			$(this).append('<img src="/images/icons/external-link-secure.png" style="padding-left: 2px;" width="10" height="10" alt="external (secure)" />');
		}
	});
	$('iframe').attr('allowfullscreen','true');
	$('iframe').attr('frameborder','0');
	$("table.striped tbody").each(function () {
		$(this).find("tr:even").addClass("odd");
	});
	$("#cn-body-inner-3col table").each(function () {
		if ($(this).width() > 750) {
			$(this).addClass('fontSize70');
		} else if ($(this).width() > 680) {
			$(this).addClass('fontSize80');
		} else if ($(this).width() > 600) {
			$(this).addClass('fontSize90');
		}
	});
	$("#cn-body-inner-2col table").each(function () {
		if ($(this).width() > 950) {
			$(this).addClass('fontSize70');
		} else if ($(this).width() > 820) {
			$(this).addClass('fontSize80');
		} else if ($(this).width() > 750) {
			$(this).addClass('fontSize90');
		}
	});
});


/**
* Change the location in the browser without actually changing the page
* 
*/
function changeLocation() {
	var url = this.arguments[0];
	var hash = this.arguments[1] ? this.arguments[1] : '#' + url;
	var title = this.arguments[2] ? this.arguments[2] : document.title;
	var scrollTo = this.arguments[3] ? this.arguments[3] : null;
	if (typeof(window.history.pushState) == 'function') {
		window.history.pushState(null, title, url);
	} else {
		window.location.hash = hash;
	}
	if ($(scrollTo)) {
		$("html, body").animate({scrollTop: $(scrollTo).position().top},250);
	} else if (parseInt(scrollTo,10) > 0) {
		$("html, body").animate({scrollTop: scrollTo},250);
	}
	return false;
}
