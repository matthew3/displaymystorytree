<?php
	$some_name = session_name('displaymy');
	session_set_cookie_params(0, '/', '.displaymy.com');
	session_start();
	
	include('php-scripts/functions/database.php');
	require_once('php-scripts/classes/BatchQuery.php');

	include('php-scripts/functions/constants.php');
	
	function addError($label, $str)
	{
		if (!isset($_SESSION['account_errors']))
		{
			$_SESSION['account_errors'] = array();
		}
		$_SESSION['account_errors'][] = $str;
	}
	
	if (!(isset($_GET['id']) or isset($_GET['user'])))
	{
		header('http://' . $_SERVER['SERVER_NAME'] . '/index.php?err=This%20user%20does%20not%20exist.');
		exit;
	}
	
	$userID = 0;
	if (isset($_GET['id']))
	{
		$userID = $_GET['id'];
	}
	
	$userName = '';
	if (isset($_GET['user']))
	{
		$userName = $_GET['user'];
	}
	
	/**Retrieve all stories created by this user, don't worry about the story's parts for now**/
	$link = openDatabase();
	
	//get story information for current user
	$q = new BatchQuery($link);
	$q->addParamQuery("SELECT id, user FROM displaymy_db.users WHERE id=? OR user=? LIMIT 1",
						'is', array($userID, $userName));
	$user = $q->execute();
	
	$isUser = false;
	if (count($user) > 0)
	{
		$user = $user[0];
		$isUser = true;
	}
	else
	{
		addError('query','Could not retrieve account information. Please try again later.');
	}
	
	if ($q->anyErrors())
	{
		addError('database','Could not retrieve account information. Please try again later.');
	}
	
	unset($q);
	
	if ($isUser)
	{
		$userID = $user['id'];
	}
	
	$stories = array();
	$branches = array();
	if (isset($user['id']))
	{
		//get all stories started by this user
		$q = new BatchQuery($link);
		$q->addParamQuery("SELECT id, title FROM stories WHERE author_id=?",
							'i', array($userID));
		$stories = $q->execute();
		
		if ($q->anyErrors())
		{
			addError('database','Could not retrieve story information. Please try again later.');
		}
		
		unset($q);
		
		/**Get all stories participated in by this user, sorted by story_id, then date created**/
		$q = new BatchQuery($link);
		$q->addParamQuery("SELECT s.title, b.story_id, b.id, REPLACE(REPLACE(SUBSTR(b.content, 1, 100), '<p>', ''), '</p>', '') AS content FROM branches AS b, stories AS s WHERE b.author_id=? and b.story_id=s.id ORDER BY b.story_id, s.date_created, b.date_created",
							'i', array($userID));
		$branches = $q->execute();
		
		if ($q->anyErrors())
		{
			addError('database','Could not retrieve story information. Please try again later.');
		}
		
		unset($q);
	}
	
	mysqli_close($link);
?>
<!DOCTYPE html>
<html>
	<head>
		<link rel="stylesheet" type="text/css" href="http://www.displaymy.com/css/main.css" />
		<link rel="stylesheet" type="text/css" href="css/account.css" />
		<link rel="stylesheet" type="text/css" href="css/my_stories.css" />
		
		<link rel="stylesheet" type="text/css" href="modules/css/top-banner.css" />
		<link rel="stylesheet" type="text/css" href="modules/css/lower-banner.css" />
		<link rel="stylesheet" type="text/css" href="modules/css/bottom-banner.css" />
	
		<link rel="stylesheet" type="text/css" href="modules/control-panel/css/control-panel.css" />
		
		<link rel="icon" href="resources/favicon.ico" type="image/x-icon" sizes="16x16" />
		
		<title>StoryTree - <?php echo $user['user']; ?>&rsquo;s Account</title>
		
		<script src="/javascript/analytics.js"></script>
	</head>
	<body>
		<?php
			include('modules/top-banner.php');
		?>
		
		<div class="middle-banner">
			<div class="main_full">
				<div class="errors">
					<?php
						//If we have errors on this page, output them then clear them
						if (isset($_SESSION['account_errors']))
						{
					?>
							<ul>
						<?php
							foreach ($_SESSION['account_errors'] as $errorStr)
							{
								echo '<li>' . $errorStr . '</li>';
							}
						?>
							</ul>
					<?php
							unset($_SESSION['account_errors']); //clear the errors
						}
					?>
				</div>
			
				<?php
					if ($isUser)
					{
				?>
						<h1>
							<?php echo $user['user']; ?>&rsquo;s Page
						</h1>
						<div class="profile-info">
							<div class="profile-pic">
								<img src="" width="128px" height="128px" />
							</div>
							
							<div class="info">
								<h3>Information</h3>
								<table>
									<tr>
										<td>Username:</td>
										<td><?php echo $user['user']; ?></td>
									</tr>
								</table>
							</div>
						</div>
						
						<h1>
							<?php echo $user['user']; ?>&rsquo;s Stories
						</h1>
						<!-- Output all stories by the current author-->
						<div class="banner">
							<h2><?php echo $user['user']; ?>&rsquo;s Stories</h2>
							<?php
								foreach ($stories as $story)
								{
							?>
									<div class="story">
										<a class="story-title" href="story.php?id=<?php echo $story['id']; ?>"><?php echo $story['title']; ?></a>
									</div>
							<?php
								}
							?>
						</div>
						
						<div class="banner">
							<h2><?php echo $user['user']; ?>&rsquo;s Contributions</h2>
							<?php
								$prevStoryID = 0;
								$storyID = 0;
								foreach ($branches as $branch)
								{
									$storyID = $branch['story_id'];
									
									//start of a new story
									if ($storyID != $prevStoryID)
									{
										if ($prevStoryID != 0) //end the last story div, if there was one
										{
							?>
											</div>
							<?php
										}
							?>
										<div class="story">
											<a class="story-title" href="story.php?id=<?php echo $storyID; ?>"><?php echo $branch['title']; ?></a>
							<?php
										$prevStoryID = $storyID;
									}
							?>
									<a class="branch" href="story.php?id=<?php echo $storyID; ?>#<?php echo $branch['id']; ?>"><?php echo $branch['content']; ?></a>
							<?php
								}
								
								if (count($branches) > 0) //did we output at least one story block? end the last one we've outputted
								{
							?>
										</div>
							<?php
								}
							?>
						</div>
				<?php
					}
					else
					{
						echo 'This is not a valid user profile.';
					}
				?>
			</div>
		</div>
		<?php
			include('modules/bottom-banner.php');
		?>
	</body>
</html>