*Use all features from DisplayMyTutorials*
	- use accounts from DisplayMyTutorials*
	- registration, activation, request password, request login, etc are all the same
	- tutorial creation turns into story creation
	- there is no section creation, this is instead performed directly on tutorial.php
	- tutorial.php becomes story.php

- each written section has a short 1 to 2 point summary on what happened in their section. This is for users to quickly glance through a story line and start contributing, or to quickly remember
what was happening within the timeline to which they were contributing.

- algorithms made to find most-read path, best-rated path, worst-rated path, least-read path, best-shortest path, worst-shortest path, best-longest path, worst-longest path, most-read longest path, most-read shortest path, least-read longest path, least-read shortest path
	***shortest and longest means that the story has hit an actual ending.

