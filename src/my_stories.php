<?php
	$some_name = session_name('displaymy');
	session_set_cookie_params(0, '/', '.displaymy.com');
	session_start();
	
	include('php-scripts/functions/database.php');
	require_once('php-scripts/classes/BatchQuery.php');

	function addError($label, $str)
	{
		if (!isset($_SESSION['stories_errors']))
		{
			$_SESSION['stories_errors'] = array();
		}
		$_SESSION['stories_errors'][] = $str;
	}
	
	//ensure user is logged in
	include('php-scripts/functions/restriction.php');
	echo ensure_user_login(true, 'http://' . $_SERVER['SERVER_NAME'] . $_SERVER['REQUEST_URI']);
	
	/**Initialize variables**/
	$stories = array();
	
	/**Retrieve all stories created by this user, don't worry about the story's parts for now**/
	$link = openDatabase();
	
	//get all stories started by this user
	$q = new BatchQuery($link);
	$q->addParamQuery("SELECT id, title FROM stories WHERE author_id=?",
						'i', array($_SESSION['user_id']));
	$stories = $q->execute();
	
	if ($q->anyErrors())
	{
		addError('database','Could not retrieve story information. Please try again later.');
	}
	
	unset($q);
	
	/**Get all stories participated in by this user, sorted by story_id, then date created**/
	$q = new BatchQuery($link);
	$q->addParamQuery("SELECT s.title, b.story_id, b.id, REPLACE(REPLACE(SUBSTR(b.content, 1, 100), '<p>', ''), '</p>', '') AS content FROM branches AS b, stories AS s WHERE b.author_id=? and b.story_id=s.id ORDER BY b.story_id, s.date_created, b.date_created",
						'i', array($_SESSION['user_id']));
	$branches = $q->execute();
	
	if ($q->anyErrors())
	{
		addError('database','Could not retrieve story information. Please try again later.');
	}
	
	unset($q);
	
	mysqli_close($link);
?>
<!DOCTYPE html>
<html>
	<head>
		<link rel="stylesheet" type="text/css" href="http://www.displaymy.com/css/main.css" />
		<link rel="stylesheet" type="text/css" href="css/my_stories.css" />
		
		<link rel="stylesheet" type="text/css" href="modules/css/top-banner.css" />
		<link rel="stylesheet" type="text/css" href="modules/css/lower-banner.css" />
		<link rel="stylesheet" type="text/css" href="modules/css/bottom-banner.css" />
	
		<link rel="stylesheet" type="text/css" href="modules/control-panel/css/control-panel.css" />
		
		<link rel="icon" href="resources/favicon.ico" type="image/x-icon" sizes="16x16" />
		
		<title>StoryTree - My Stories</title>
	</head>
	<body>
		<?php
			include('modules/top-banner.php');
		?>
		
		<div class="middle-banner">
			<div class="main_full">
				<div class="errors">
					<?php
						//If we have errors on this page, output them then clear them
						if (isset($_SESSION['stories_errors']))
						{
					?>
							<ul>
						<?php
							foreach ($_SESSION['stories_errors'] as $errorStr)
							{
								echo '<li>' . $errorStr . '</li>';
							}
						?>
							</ul>
					<?php
							unset($_SESSION['stories_errors']); //clear the errors
						}
					?>
				</div>
			
				<h1>
					My Stories
				</h1>
				
				<div style="width:200px; margin:0px auto;">
					<a class="reg-button" style="width:170px;" href="new_story.php?action=new">New Story</a>
				</div>
				
				<!-- Output all stories by the current author-->
				<div class="banner">
					<h2>My Stories</h2>
					<?php
						foreach ($stories as $story)
						{
					?>
							<div class="story">
								<a class="story-title" href="story.php?id=<?php echo $story['id']; ?>"><?php echo $story['title']; ?></a>
							</div>
					<?php
						}
					?>
				</div>
				
				<div class="banner">
					<h2>My Contributions</h2>
					<?php
						$prevStoryID = 0;
						$storyID = 0;
						foreach ($branches as $branch)
						{
							$storyID = $branch['story_id'];
							
							//start of a new story
							if ($storyID != $prevStoryID)
							{
								if ($prevStoryID != 0) //end the last story div, if there was one
								{
					?>
									</div>
					<?php
								}
					?>
								<div class="story">
									<a class="story-title" href="story.php?id=<?php echo $storyID; ?>"><?php echo $branch['title']; ?></a>
					<?php
								$prevStoryID = $storyID;
							}
					?>
							<a class="branch" href="story.php?id=<?php echo $storyID; ?>#<?php echo $branch['id']; ?>"><?php echo $branch['content']; ?></a>
					<?php
						}
						
						if (count($branches) > 0) //did we output at least one story block? end the last one we've outputted
						{
					?>
								</div>
					<?php
						}
					?>
				</div>
				
			</div>
		</div>
		
		<?php
			include('modules/bottom-banner.php');
		?>
	</body>
</html>