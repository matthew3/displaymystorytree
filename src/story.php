<?php
	$some_name = session_name('displaymy');
	session_set_cookie_params(0, '/', '.displaymy.com');
	session_start();
	
	include('php-scripts/functions/database.php');
	include('php-scripts/classes/BatchQuery.php');
	
	include('php-scripts/functions/restriction.php');
	include('php-scripts/functions/constants.php');
	
	function addError($label, $str)
	{
		if (!isset($_SESSION['story_errors']))
		{
			$_SESSION['story_errors'] = array();
		}
		$_SESSION['story_errors'][] = $str;
	}
	
	$link = openDatabase();
	
	//get the story ID whether by id or branch_id
	$storyID = 0;
	if (isset($_GET['id']))
	{
		$storyID = intval($_GET['id']);
	}
	else if (isset($_GET['branch_id']))
	{
		$branchID = $_GET['branch_id'];
	
		$q = new BatchQuery($link);
		$q->addParamQuery("SELECT story_id FROM branches WHERE id=?",
							'i', array($branchID));
		$storyID = $q->execute();
		
		if ($q->anyErrors())
		{
			addError('database','Could not retrieve story information. Please try again later.');
		}
		
		if (empty($storyID) or $storyID == null)
		{
			mysqli_close($link);
			header('Location: story_not_found.php');
			exit();
		}
		
		$storyID = intval($storyID[0]['story_id']);
		
		mysqli_close($link);
		header('Location: story.php?id=' . $storyID . '#' . $branchID, true, 302);
		exit();
		
		unset($q);
	}
	
	//must provide some story id
	if ($storyID == 0)
	{
		header('Location: story_not_found.php');
		exit();
	}
	
	//get all story information
	$q = new BatchQuery($link);
	$q->addParamQuery("SELECT s.id, s.trunk, s.author_id, s.title, c1.description AS primary_cat_desc, s.primary_cat, c2.description AS secondary_cat_desc, s.secondary_cat
									FROM stories AS s, categories AS c1, categories AS c2
									WHERE s.id=? AND s.primary_cat = c1.id AND s.secondary_cat = c2.id",
						'i', array($storyID));
	$story = $q->execute();
	
	if ($q->anyErrors())
	{
		addError('database','Could not retrieve story information. Please try again later.');
	}
	
	if (empty($story) or $story == null)
	{
		mysqli_close($link);
		header('Location: story_not_found.php');
		exit();
	}
	
	unset($q);
	
	$IS_AUTHOR = false;
	$story = $story[0];
	
	if (isset($_SESSION['user_id']) and $story['author_id'] == $_SESSION['user_id'])
	{
		$IS_AUTHOR = true;
	}
	
	mysqli_close($link);
?>
<!DOCTYPE html>
<html lang="en">
	<head>
		<link rel="stylesheet" type="text/css" href="http://www.displaymy.com/css/main.css" />
		<link rel="stylesheet" type="text/css" href="css/story.css" />
		
		<link rel="stylesheet" type="text/css" href="modules/css/top-banner.css" />
		<link rel="stylesheet" type="text/css" href="modules/css/lower-banner.css" />
		<link rel="stylesheet" type="text/css" href="modules/css/bottom-banner.css" />
	
		<link rel="stylesheet" type="text/css" href="modules/control-panel/css/control-panel.css" />
		
		<link rel="icon" href="resources/favicon.ico" type="image/x-icon" sizes="16x16" />
		
		<title>StoryTree - Story<?php echo (isset($story['title'])? (' - ' . $story['title']) : ''); ?></title>
		
		<meta charset="utf-8">
		
		<script type="text/javascript">
			var idPath = new Array("0");
		</script>
		
	</head>
	<body>
		<!-- Add facebook likes to the page -->
		<div id="fb-root"></div>
		<script>
			  window.fbAsyncInit = function() {
				FB.init({
				  appId      : '307484996089864',
				  xfbml      : false,
				  version    : 'v2.0'
				});
			  };

			  (function(d, s, id){
				 var js, fjs = d.getElementsByTagName(s)[0];
				 if (d.getElementById(id)) {return;}
				 js = d.createElement(s); js.id = id;
				 js.src = "//connect.facebook.net/en_US/sdk.js";
				 fjs.parentNode.insertBefore(js, fjs);
			   }(document, 'script', 'facebook-jssdk'));
		</script>
	
		<?php
			include('modules/top-banner.php');
		?>
		
		<div class="middle-banner">
			<div class="main_full">
			
				<div id="category_area">
					<a class="category" href="category.php?cat_id=<?php echo $story['primary_cat'] ?>">
						<?php echo $story['primary_cat_desc']; ?>
					</a>
					
					<a class="category" href="category.php?cat_id=<?php echo $story['secondary_cat'] ?>">
						<?php echo $story['secondary_cat_desc']; ?>
					</a>
				</div>
			
				<div class="story">
			
					<?php
						if (!empty($story['trunk']))
						{
							echo '<div class="title">';
							echo $story['title'];
							echo '</div>';
							
							echo '<div class="trunk">';
							echo $story['trunk'];
							echo '</div>';
					?>
							<div id="past_branches"> <!--This is where past branches will be inserted through javascript-->
							</div>
							
							<script type="text/javascript">
								function show_read_area()
								{
									document.getElementById('read_area').style.display = '';
									document.getElementById('read').style.backgroundColor = '#999999';
									
									document.getElementById('write').style.backgroundColor = '#DDDDDD';
									document.getElementById('write_area').style.display = 'none';
								}
								
								function show_write_area()
								{
									document.getElementById('write_area').style.display = '';
									document.getElementById('write').style.backgroundColor = '#999999';
									
									document.getElementById('read').style.backgroundColor = '#DDDDDD';
									document.getElementById('read_area').style.display = 'none';
								}
								
								function post_branch()
								{
									$.post("php-scripts/create_branch.php", {story_id: <?php echo $story['id']; ?>,
																			ending: ((document.getElementById('ending').checked) ? 'true' : 'false'),
																			parent_id: '' + parentID,
																			content: document.getElementById('branch_text').value,
																			winnie: document.getElementById('branch_winnie').value},
											function(result)
											{
												var i = result.indexOf('+');
												var id = result.substr(0, i);
												var content = result.substr(i+1, result.length);
												
												branches.push(content);
												keys.push(id);
												navigateAbsolute(branches.length-1);
												
												show_read_area();
												
												FB.XFBML.parse();
											});
								}
							</script>
							
							<div id="action_area">
								<div id="read_area">
									<div id="branches"> <!--This is where branches will be inserted through ajax-->
										<br /><!-- To keep the spacing correct-->
									</div>
									
									<div id="nav">
										<div id="pages">
											<button id="left_nav_btn" class="nav_btn" onclick="navigateRelative(-1)">
												&lt;
											</button>
											
											<div id="page_num">
											</div>
											
											<button id="right_nav_btn" class="nav_btn" onclick="navigateRelative(1)">
												&gt;
											</button>
										
											<div id="buttons_area">
												Actions
												<br />
												<button onclick="get_next_level(keys[selectedBranch], true, true)">Read More</button>
												<button onclick="up_one_level()">Up One</button>
											</div>
										</div>
									</div>
								</div>
								
								<div id="write_area">
									<div id="my_branch">
										<span id="write_title">Make the story your own!</span>
										<form id="submit_branch" onsubmit="post_branch()">
											<input type="hidden" name="winnie" id="branch_winnie" value="" />
											<span id="text_area">
												<textarea id="branch_text" maxlength="5000" style="width:75%; height:250px;" ></textarea>
											</span>
											<br />
											<input type="checkbox" id="ending" name="ending" value="true" />This is an ending
											<input type="button" onclick="document.getElementById('submit_branch').onsubmit()" value="Publish" />
										</form>
									</div>
									<div id="is_ending">
										This is an ending. You cannot continue an ending.
									</div>
								</div>
							</div>
							
							<div id="action">
								<div id="read" onclick="show_read_area()">
									Read
								</div>
								<div id="write" onclick="show_write_area()">
									Write
								</div>
							</div>
					<?php
						}
						else if ($IS_AUTHOR) //empty and is_author
						{
					?>
							<div id="trunk">
								Please post the trunk of your story that will start the tree.
								<script type="text/javascript">
									function post_trunk()
									{
										$.post("php-scripts/create_trunk.php", {story_id: '<?php echo $story['id']; ?>',
																				trunk: document.getElementById('trunk_text').value,
																				winnie: document.getElementById('trunk_winnie').value},
												function(result)
												{
													location.href='story.php?id=<?php echo $story['id']; ?>';
													FB.XFBML.parse();
												});
									}
								</script>
								<div id="submit_trunk">
									<input type="hidden" id="trunk_winnie" value="" />
									<textarea id="trunk_text" maxlength="10000" style="width:99%; height:500px;"></textarea>
									<br />
									<button onclick="post_trunk()">Publish</button>
								</div>
							</div>
					<?php
						}
						else //empty and not author
						{
							echo 'This story has not been started yet. Please wait for the author to start it off.';
						}
					?>
				
					<script type="text/javascript">
						var selectedBranch = 0;
						var branches = null;
						var keys = null;
						var parentID = 0;
						var processing = false;
						
						//by using branchID with default value 0 (when no branch_id is specified)
						//we can get the next level of branches from a user-specified branch
						<?php
							if (!empty($story['trunk']))
							{
						?>
								window.onload = function(){
										$( document ).ready(function(){
										
											if (window.location.hash)
											{
												var branch_hash = window.location.hash.slice(1);
												$.post("php-scripts/get_past_branches.php", {branch_id: branch_hash},
													function(result)
													{
														var i = result.indexOf('+');
														var pushes = result.substr(0, i);
														var content = result.substr(i+1, result.length);
													
														eval(pushes);
														document.getElementById('past_branches').innerHTML += content;
														
														get_next_level(branch_hash, false);
														FB.XFBML.parse();
													});
											}
											else
											{
												get_next_level(0, false);
											}
											
											show_read_area();
										});
									}
						<?php
							}
						?>
						
						//resets navigation back to 0-index
						function reset_branch_nav(isEnding)
						{
							if (branches == null)
								return;
								
							selectedBranch = 0;
							var branchDiv = document.getElementById('branches');
							var writeTitleSpan = document.getElementById('write_title');
							
							document.getElementById('my_branch').style.display = '';
							document.getElementById('is_ending').style.display = 'none';
							
							//set the textarea to the default textbox
							document.getElementById('text_area').innerHTML = '<textarea id="branch_text" maxlength="5000" style="width:75%; height:90px;" ></textarea>';
							
							if (branches != null && branches.length > 0)
							{
								branchDiv.innerHTML = branches[selectedBranch];
								writeTitleSpan.innerHTML = 'Make the story your own!';
							}
							else if (branches.length == 0)
							{
								if (isEnding == '1') //is an ending
								{
									document.getElementById('my_branch').style.display = 'none';
									document.getElementById('is_ending').style.display = '';
									branchDiv.innerHTML = "Nothing to show.";
								}
								else
								{
									branchDiv.innerHTML = '... To Be Continued By You!';
									writeTitleSpan.innerHTML = 'Continue this story!';
								}
							}
							else
							{
								branchDiv.innerHTML = '';
							}
							
							navigateAbsolute(0);
							
							document.getElementById('page_num').innerHTML = (selectedBranch+1) + '/' + branches.length;
						}
						
						function set_to_branch(branchID)
						{
							console.log("Start: " + idPath);
						
							var count = -1;
							
							for (var i = idPath.length-1; i > 0; i--)
							{
								if (idPath[i] != branchID)
								{
									remove_past_branch_by_id(idPath[i]);
								}
								else
								{
									remove_past_branch_by_id(branchID);
									break;
								}
							}
							
							console.log("After removing: " + idPath);
							
							get_next_level(idPath[idPath.length-1], false, false);
							navigate_to_id(branchID);
							
							console.log("Ending: " + idPath);
						}
						
						function up_one_level()
						{
							if (idPath.length == 1)
							{
								return;
							}
							
							var currentID = idPath.pop();
							
							remove_past_branch_by_id(currentID);
							
							var parent = idPath[idPath.length-1];
							
							get_next_level(parent, false, false);
							navigate_to_id(currentID);
						}
						
						//use ajax call to get next pieces of story
						function get_next_level(parent, putToPast, addToPath)
						{
							if (parent == 'undefined' || parent == null)
							{
								return;
							}
							
							if (!processing)
							{
								//add a view to any branch read (except for 0 - the trunk)
								if (parent != 0)
								{
									$.post("php-scripts/view.php", {branch_id: parent},
												function(result)
												{
												});
								}
							
								$.post("php-scripts/get_next_level.php", {story_id: '<?php echo $story['id']; ?>',
																						parent_id: '' + parent},
										function(result)
										{
											if (result.length == '')
											{
												return;
											}
											
											//save a text version of the branch in past-branches, then continue to load new branches
											//only if it is successful
											if (branches != null && branches.length > selectedBranch)
											{
												if (putToPast)
												{
													var pastBranches = document.getElementById('past_branches');
													var text = branches[selectedBranch];
													if (text != 'undefined')
													{
														pastBranches.innerHTML += text;
													}
												}
											}
											
											//ending logic
											isEnding = '0';
											if (result == '1')
											{
												isEnding = '1';
												branches = [];
												keys = [];
											}
											else
											{
												//get new branches, eval performs commented code below
												eval(result);
												//var branchArray = json_encode($branches);
												//var keyArray = json_encode($keys);
												branches = branchArray;
												keys = keyArray;
											}
												
											parentID = parent;
											if (addToPath)
											{
												if (parentID != 0)
												{
													idPath.push('' + parentID);
												}
											}
											
											reset_branch_nav(isEnding);
											window.location.hash = '#' + parentID;
											processing = false;
											
											FB.XFBML.parse();
										});
							}
							
							processing = true;
						}
						
						function remove_past_branch_by_id(id)
						{
							console.log("removing: " + id);
						
							if (branches == null)
								return;
						
							//remove the last branch from past_branches
							var lastBranch = document.getElementById('branch' + id);
							if (lastBranch == null || lastBranch == 'undefined')
							{
								return;
							}
							lastBranch.parentElement.removeChild(lastBranch);
							
							//remove from path array
							var index = idPath.indexOf(''+id); //array elements are strings....
							if (index > -1)
							{
								idPath.splice(index);
								console.log("after slice: " + idPath);
							}
							else
							{
								console.log("failed to remove index: " + index);
							}
						}
						
						function navigate_to_id(id)
						{
							//navigate to the index that they were previously looking at
							for (var i = 0; i < keys.length; i++)
							{
								if (keys[i] == id)
								{
									navigateAbsolute(i);
								}
							}
						}
						
						//accepts navigate(1) to go right and navigate(-1) to go left or navigate(-5) to go 5 left
						function navigateRelative(direction)
						{
							newSelectedBranch = selectedBranch + direction;
							selectedBranch = limitSelected(newSelectedBranch);

							navigateAbsolute(selectedBranch);
						}
						
						function navigateAbsolute(newSelectedBranch)
						{
							selectedBranch = limitSelected(newSelectedBranch);
							
							if (branches.length > 0)
							{
								document.getElementById('branches').innerHTML = branches[selectedBranch];
								document.getElementById('page_num').innerHTML = (selectedBranch+1) + '/' + branches.length;
							}
						}
						
						function limitSelected(newSelectedBranch)
						{
							if (newSelectedBranch < 0)
							{
								return 0;
							}
							else if (newSelectedBranch >= branches.length)
							{
								return (branches.length-1);
							}
							else
							{
								return newSelectedBranch;
							}
						}
						
						function toggleNavButtons()
						{
							if (selectedBranch > 0)
							{
								document.getElementById('left_nav_btn').style.display = '';
							}
							else
							{
								document.getElementById('left_nav_btn').style.display = 'none';
							}
							
							if (selectedBranch < branches.length)
							{
								document.getElementById('right_nav_btn').style.display = '';
							}
							else
							{
								document.getElementById('right_nav_btn').style.display = 'none';
							}
						}
						
						function cast_vote(branchID)
						{
							$.post("php-scripts/vote.php", {branch_id: branchID},
								function(result)
								{
									if (result == '1')
									{
										var branch = document.getElementById('branch_rating' + branchID);
										
										if (branch == null)
										{
											return;
										}
											
										var rating = parseInt(branch.innerHTML);
										rating = rating + 1;
										branch.innerHTML = rating;
										branch.style.color = '#009900';
									}
								});
						}
					</script>
			
				</div> <!-- end of story -->
			
			</div> <!-- end of mail_full -->

		</div>
		
		<?php
			include('modules/bottom-banner.php');
		?>
	</body>
</html>